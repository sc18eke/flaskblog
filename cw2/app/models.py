from app import db
from flask_login import LoginManager, UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

# users = db.Table('user_blogs',
# db.Column('user_id',db.Integer,db.ForeignKey('user.id')),
# db.Column('post_id',db.Integer,db.ForeignKey('post.id'))
# )

class Post(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    title = db.Column(db.String(50))
    subtitle = db.Column(db.String(50))
    author_id = db.Column(db.Integer,db.ForeignKey('user.id')) 
    date_posted = db.Column(db.DateTime)
    content = db.Column(db.Text)    

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique = True)
    email = db.Column(db.String(50),unique = True)
    password = db.Column(db.String(80), unique = True)
    posts = db.relationship('Post',backref='author',lazy='select')


    @staticmethod
    def get_reset_token(self,expires_sec=1800):
        s = Serializer(app.config['SECRET_KEY'],expires_sec)
        return s.dumps({'used_id': self.id}).decode('utf-8')

    def verify_reset_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.Query.get(user_id)       



def set_password(self, password):     
    self.password = generate_password_hash(password, method='sha256')

def check_password(self, password):
    return check_password_hash(self.password, password)

def __repr__(self):
    return '<User {}>'.format(self.username)
