from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, IntegerField, SubmitField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email, Length,EqualTo, ValidationError
from wtforms.widgets import TextArea
from .models import User

class LoginForm(FlaskForm):
    name = StringField('Name',validators=[DataRequired(), Length(min=3,max=15)],  render_kw={"placeholder": "Name"})
    password = PasswordField('Password',validators=[DataRequired(), Length(min=6,max=80)],  render_kw={"placeholder": "Password"})

class RegisterForm(FlaskForm):
    email = StringField('E-mail',validators = [DataRequired(),Email(message = 'Invalid e-mail address!')],render_kw={"placeholder": "E-mail"})
    name = StringField('Name',validators=[DataRequired(), Length(min=8,max=60)],render_kw={"placeholder": "Name"})
    password = PasswordField('Password',validators=[DataRequired(), Length(min=6,max=80)],render_kw={"placeholder": "Password"})

class PostForm(FlaskForm):
    title = StringField('Title',validators = [DataRequired()], render_kw={"placeholder": "Title"})
    subtitle = StringField('Subtitle',validators = [DataRequired()], render_kw={"placeholder": "Subtitle"})
    author = StringField('Author')
    content = StringField('Blog Content',validators = [DataRequired()], render_kw={"placeholder": "Content"},  widget=TextArea())

class RequestResetForm(FlaskForm):
    email = StringField('Email',validators = [DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')

    def validate_email(self,email):
        user = User.query.filter_by(email=email.data).first()
        if user is None:
            raise ValidationError('There is no accoint with that email.You must register first')

class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')

