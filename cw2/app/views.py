from flask import Flask, session, render_template, flash, request, url_for, redirect, abort, request, make_response, g
from app import app,db, mail
from .forms import LoginForm, RegisterForm, PostForm, RequestResetForm, ResetPasswordForm
from flask_sqlalchemy import SQLAlchemy
from app.models import User, Post
from flask_login import LoginManager, UserMixin, login_user,login_required, logout_user, current_user
from flask_mail import Mail, Message
from werkzeug.security import generate_password_hash, check_password_hash
from flask_bootstrap import Bootstrap
from datetime import datetime
import smtplib
import time


login_manager = LoginManager( )
login_manager.init_app(app)
login_manager.login_view = 'login'
# mail = Mail(app)

@app.before_request
def before_request():
    g.user = current_user
    


@login_manager.user_loader
def load_user(user_id):
    if user_id is not None:
        return User.query.get(user_id)
    return None

@login_manager.unauthorized_handler
def unauthorized():
    
    flash('You must be logged in to view that page.')
    return redirect(url_for('login'))    

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/home')
def home():
    if current_user.is_authenticated:
        posts = Post.query.order_by(Post.date_posted.desc()).all()
        return render_template('home.html', posts = posts)
    return render_template('index.html')

    # @app.route("/user/<string:username>")
    # def myposts(username):
    #     users = User.query.filter_by(username=name).first()
    #     posts = Post.query.filter_by(author=users).order_by(Post.date_posted.desc())
        
    #     return render_template('myposts.html',users=users, posts = posts)

@app.route('/post/<int:post_id>')
def post(post_id):
    post = Post.query.filter_by(id=post_id).one()
    if current_user.is_authenticated:
        return render_template('post.html', post = post)
    return redirect(url_for('login'))        

         
@app.route('/add')
@login_required
def add():
    form = PostForm()

    return render_template('add.html',form = form)

#route for handling the submition of the post
@app.route('/addedpost', methods = ['POST'])
def addedpost():
    form = PostForm()
    if form.validate_on_submit():  
        print(form.validate_on_submit())
        author = g.user.id   
        print(author)  
        new_post = Post(title=form.title.data,subtitle=form.subtitle.data,author_id = author, date_posted = datetime.now(),content=form.content.data)
        print(new_post)
        db.session.add(new_post) # add the new post with that author
        # author.posts.append(new_post)
        db.session.commit()
        return redirect(url_for('home'))
    return render_template('add.html', form=form)    

#LOGIN FUNCTION
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    login_form = LoginForm(request.form)
    # POST: Create user and redirect them to the app
    if request.method == 'POST':
        session.pop('user_id',None)

        if login_form.validate():
            # Get Form Fields
            name = request.form.get('name')
            password = request.form.get('password')
            # Validate Login Attempt
            user = User.query.filter_by(name=name).first()
            if user:
                if check_password_hash(user.password,login_form.password.data):
                    session['user_id'] = user.id
                    login_user(user)
                    print(session)
                    next = request.args.get('next')
                    return redirect(next or url_for('home'))
        flash('Invalid username/password combination')
        return redirect(url_for('login'))
    return render_template('login.html', form=login_form)

#LOGOUT FUNCTION
@app.route('/logout')
@login_required
def logout():
    logout_user() #login manager command
    flash('You have been logged out!', 'info')
    return redirect(url_for('index')) # going back to home page    

#REGISTER FUNCTION
@app.route('/signin', methods=['GET', 'POST'])
def signin():
    signup_form = RegisterForm(request.form)
    # POST: Sign user in
    if request.method == 'POST':
        if signup_form.validate():
            # Get Form Fields
            name = request.form.get('name')
            email = request.form.get('email')
            password = request.form.get('password')
            existing_user = User.query.filter_by(email=email).first()
            if existing_user is None:
                user = User(name=name,
                            email=email,
                            password=generate_password_hash(password, method='sha256'))
                db.session.add(user)
                db.session.commit()
                #login_user(user) #logs the user in after the registration
                return redirect(url_for('home'))
            flash('A user already exists with that email address.')
            return redirect(url_for('signin')) 
    return render_template('signin.html', form=signup_form)

# def send_reset_email(user):
#     token = user.get_reset_token()
#     msg = Message('Password Reset Request', sender='noreply@demo.com',recipients=[user.email])
#     msg.body = " Hi.. If you want to reset your email"
#     mail.send(msg)

# @app.route('/reset_password',methods=["GET", "POST"])
# def reset_request():
#     if current_user.is_authenticated:
#         return redirect(url_for('home'))
#     form = RequestResetForm()
#     if form.validate_on_submit():
#         user = User.query.filter_by(email=form.email.data).first()
#         send_reset_email(user)
#         flash('An email has been sent with instructions to reset your password.','info')
#         return redirect(url_for('login'))
#     return render_template('reset_request.html',title ='Reset Password', form = form)

# @app.route('/reset_password/<token>',methods=["GET", "POST"])
# def reset_token(token):
#     if current_user.is_authenticated:
#         return redirect(url_for('home'))    
#     user = User.verify_reset_token(token)
#     if user is None:
#         flash('That is an invalid or expired token!', 'warning')
#         return redirect(url_for('reset_request'))
#     form = ResetPasswordForm()
#     if form.validate():
#         hashed_pass = password=generate_password_hash(form.password.data, method='sha256')
#         user.password = hashed_pass
#         db.session.commit()
#         flash('Your password has been updated!')
#         return redirect(url_for('home'))
#     return render_template('reset_token.html',title='Reset Password',form = form)     






 