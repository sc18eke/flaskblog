import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_mail import Mail, Message
from flask_bootstrap import Bootstrap

app = Flask(__name__)

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 587
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = os.environ.get('DB_USERNAME')
app.config['MAIL_PASSWORD'] = os.environ.get('DB_PASS')


app.config['SECRET_KEY'] = 'verysecretkey'
app.config.from_object('config')
db = SQLAlchemy(app)
mail = Mail(app)
bootstrap = Bootstrap(app)
migrate = Migrate(app, db)

from app import views, models
